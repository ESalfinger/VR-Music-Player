﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnviromentLoader : MonoBehaviour
{
    public Animator transition;

    public float transitonTime = 1f;

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadEnviroment(sceneName));
    }

    IEnumerator LoadEnviroment(string sceneName)
    {
        transition.SetTrigger("Start");
        
        yield return new WaitForSeconds(transitonTime);
            
        SceneManager.LoadScene(sceneName);

    }
    
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Application closing");
    }
}
